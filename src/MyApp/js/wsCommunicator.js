var wsCommunicator = function(custom_options){

    var options = {
        port: 8082,
        url: 'ws://localhost',
        onopen: function(){},
        onmessage: function(){}
    };

    $.extend(options,custom_options);

    var connection = new WebSocket(options.url+':'+options.port);

    connection.onopen = function(e) {
        console.log("Connection established!");
        options.onopen(e);
/*        conn.send('Hello World!');*/
    };

    connection.onmessage = function(e){
        var json = JSON.parse(e.data);
        console.log('recieved JSON: ',json);
        options.onmessage(json);
    };

    this.send = function(json){
        var str = JSON.stringify(json);
        console.log('sent JSON str: ',str);
        connection.send(str);
    }



};