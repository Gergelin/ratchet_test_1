<?php
/**
 * Created by PhpStorm.
 * User: Th�n Gergely
 * Date: 2015. 11. 09.
 * Time: 15:22
 */

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use MyApp\Chat;

    require dirname(__DIR__) . '/vendor/autoload.php';

    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8082
    );

    $server->run();